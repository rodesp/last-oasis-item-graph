FROM node:lts AS build
ENV NODE_ENV=production

WORKDIR /build
COPY . .

RUN npm install --production
RUN npm run build

FROM nginx:stable-alpine

WORKDIR /usr/share/nginx/html

COPY --from=build /build/build .

COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]
