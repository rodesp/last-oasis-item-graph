import Cytoscape from 'cytoscape';
import dagre from 'cytoscape-dagre';
import fcose from 'cytoscape-fcose';
import klay from 'cytoscape-klay';
import cyUR from 'cytoscape-undo-redo';
import cyViewUtilities from 'cytoscape-view-utilities';
import React, { useEffect, useState } from 'react';
import CytoscapeComponent from 'react-cytoscapejs';

import log from '../utilities/logger';
import { breadthfirst } from './layouts/layouts'

cyUR(Cytoscape);
cyViewUtilities(Cytoscape);
Cytoscape.use(dagre);
Cytoscape.use(klay);
Cytoscape.use(fcose);

const CyComponent = (props) => {
	let cyRef;
	const [items, setItems] = useState(props.items);

	useEffect(() => {
		const cy = cyRef;

		const ur = cy.undoRedo();
		const api = cy.api = cy.viewUtilities({
			highlightStyles: [
				{ node: { 'border-color': '#000000', 'border-width': 3 }, edge: { 'line-color': '#000000', 'source-arrow-color': '#000000', 'target-arrow-color': '#000000', 'width': 3 } },
			],
			selectStyles: {
				node: { 'border-color': 'black', 'border-width': 3, 'background-color': 'lightgrey' },
				edge: { 'line-color': 'black', 'source-arrow-color': 'black', 'target-arrow-color': 'black', 'width': 3 }
			},
			setVisibilityOnHide: false, // whether to set visibility on hide/show
			setDisplayOnHide: true, // whether to set display on hide/show
			zoomAnimationDuration: 1500, //default duration for zoom animation speed
			neighbor: (node) => node.closedNeighborhood(),
			neighborSelectTime: 1000
		});

		const breadthfirstLayout = cy.breadthfirstLayout = cy.layout(
			{
				...breadthfirst,
				roots: cy.$('.root'),
			}
		);
		// const concentricLayout = cy.concentricLayout = cy.layout();
		// const fcoseLayout = cy.fcoseLayout = cy.layout();
		// const dagreLayout = cy.dagreLayout = cy.layout();
		// const klayLayout = cy.klayLayout = cy.layout();

		cy.setLayout = (newLayout) => {
			cy.layoutObject = newLayout;
			return cy.layoutObject;
		};

		try {
			cy.setLayout(breadthfirstLayout).run();
		} catch (e) {
			log.error(e);
		}

		// function thickenBorder (eles) {
		// 	eles.forEach((ele) => { ele.css('background-color', 'purple'); });
		// 	eles.data('thickBorder', true);
		// 	return eles;
		// }
		// Decrease border width when hidden neighbors of the nodes become visible
		// function thinBorder (eles) {
		// 	eles.forEach((ele) => { ele.css('background-color', 'green'); });
		// 	eles.removeData('thickBorder');
		// 	return eles;
		// }
		// ur.action('thickenBorder', thickenBorder, thinBorder);
		// ur.action('thinBorder', thinBorder, thickenBorder);

		// document.getElementById('searchBox').addEventListener('keyup', function (event) {
		// 	// Number 13 is the 'Enter' key on the keyboard
		// 	if (event.keyCode === 13) {
		// 		// Cancel the default action, if needed
		// 		event.preventDefault();
		// 		// Trigger the button element with a click
		// 		document.getElementById('searchButton').click();
		// 	}
		// });

		const remove = function (eles) {
			eles = eles.union(eles.connectedEdges());
			eles.unselect();

			// removedElements.push(eles);
			eles.remove();

			return eles;
		};

		//In below functions, finding the nodes to hide/show are sample specific.
		//If the sample graph changes, those calculations may also need a change.
		document.getElementById('hide').addEventListener('click', () => {
			api.disableMarqueeZoom();
			const actions = [];
			const nodesToHide = cy.$(':selected').add(cy.$(':selected').nodes().descendants());
			// let nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes().intersection(nodesToHide);
			// actions.push({ name: 'thinBorder', param: nodesWithHiddenNeighbor });
			actions.push({ name: 'hide', param: nodesToHide });
			// nodesWithHiddenNeighbor = nodesToHide.neighborhood(':visible').nodes().difference(nodesToHide).difference(cy.nodes('[thickBorder]'));
			// actions.push({ name: 'thickenBorder', param: nodesWithHiddenNeighbor });
			ur.do('batch', actions);
			if (document.getElementById('layout').checked) {
				cy.layoutObject.run();
			}
		});

		document.getElementById('hideUnselected').addEventListener('click', () => {
			api.disableMarqueeZoom();
			const actions = [];
			const nodesToHide = cy.$(':unselected').add(cy.$(':unselected').nodes().descendants());
			// let nodesWithHiddenNeighbor = cy.edges(':hidden').connectedNodes().intersection(nodesToHide);
			// actions.push({ name: 'thinBorder', param: nodesWithHiddenNeighbor });
			actions.push({ name: 'hide', param: nodesToHide });
			// nodesWithHiddenNeighbor = nodesToHide.neighborhood(':visible').nodes().difference(nodesToHide).difference(cy.nodes('[thickBorder]'));
			// actions.push({ name: 'thickenBorder', param: nodesWithHiddenNeighbor });
			ur.do('batch', actions);
			if (document.getElementById('layout').checked) {
				cy.setLayout(breadthfirstLayout).run();
			}
		});

		document.getElementById('showAll').addEventListener('click', () => {
			api.disableMarqueeZoom();
			const actions = [];
			// const nodesWithHiddenNeighbor = cy.nodes('[thickBorder]');
			// actions.push({ name: 'thinBorder', param: nodesWithHiddenNeighbor });
			actions.push({ name: 'show', param: cy.elements() });
			ur.do('batch', actions);
			if (document.getElementById('layout').checked) {
				cy.layoutObject.run();
			}
		});

		document.getElementById('showHiddenNeighbors').addEventListener('click', () => {
			api.disableMarqueeZoom();
			// const hiddenEles = cy.$(':selected').neighborhood().filter(':hidden');
			const selectedNodes = cy.nodes(':selected');
			// let nodesWithHiddenNeighbor = (hiddenEles.neighborhood(':visible').nodes('[thickBorder]')).difference(cy.edges(':hidden').difference(hiddenEles.edges().union(hiddenEles.nodes().connectedEdges())).connectedNodes());
			const actions = [];
			// actions.push({ name: 'thinBorder', param: nodesWithHiddenNeighbor });
			actions.push({ name: 'showHiddenNeighbors', param: selectedNodes });
			// nodesWithHiddenNeighbor = hiddenEles.nodes().edgesWith(cy.nodes(':hidden').difference(hiddenEles.nodes())).connectedNodes().intersection(hiddenEles.nodes());
			// actions.push({ name: 'thickenBorder', param: nodesWithHiddenNeighbor });

			ur.do('batch', actions);
			if (document.getElementById('layout').checked) {
				cy.layoutObject.run();
			}
		});

		document.getElementById('marqueeZoom').addEventListener('click', () => {
			api.enableMarqueeZoom();
			if (document.getElementById('layout').checked) {
				cy.layoutObject.run();
			}
		});

		let tappedBefore;
		cy.on('tap', 'node', (event) => {
			const node = event.target;
			const tappedNow = node;
			setTimeout(() => {
				tappedBefore = null;
			}, 300);
			if (tappedBefore) {
				if (tappedBefore.id() === tappedNow.id()) {
					tappedNow.trigger('doubleTap');
					tappedBefore = null;
				}
			} else {
				tappedBefore = tappedNow;
			}
		});

		cy.on('doubleTap', 'node', (event) => {
			api.disableMarqueeZoom();
			const selectedNode = event.target;
			let ingredientNodes = cy.nodes().edgesTo(selectedNode).connectedNodes();
			ingredientNodes = ingredientNodes.add(cy.nodes().edgesTo(selectedNode))
			const actions = [];

			// actions.push({ name: 'thinBorder', param: ingredientNodes });
			actions.push({ name: 'show', param: ingredientNodes });
			// actions.push({ name: 'thickenBorder', param: ingredientNodes });

			ur.do('batch', actions);
			if (document.getElementById('layout').checked) {
				cy.layoutObject.run();
			}
		});

		document.getElementById('highlightNeighbors').addEventListener('click', () => {
			if (cy.$(':selected').length > 0) {
				ur.do('highlightNeighbors', { eles: cy.$(':selected'), idx: document.getElementById('highlightColors').selectedIndex });
			}
		});

		document.getElementById('highlightElements').addEventListener('click', () => {
			if (cy.$(':selected').length > 0) {
				ur.do('highlight', { eles: cy.$(':selected'), idx: document.getElementById('highlightColors').selectedIndex });
			}
		});

		document.getElementById('removeSelectedHighlights').addEventListener('click', () => {
			const selected = cy.$(':selected');
			if (selected.length > 0) {
				ur.do('removeHighlights', selected);
				selected.unselect();
			}
		});

		document.getElementById('removeAllHighlights').addEventListener('click', () => {
			ur.do('removeHighlights');
		});

		document.getElementById('highlightColors').addEventListener('change', (e) => {
			document.getElementById('colorInp').value = e.target.value;
		});

		document.getElementById('colorChangerBtn').addEventListener('click', () => {
			let color = document.getElementById('colorInp').value;
			const idx = document.getElementById('highlightColors').selectedIndex;
			let nodeStyle = { 'border-color': color, 'border-width': 3 };
			let edgeStyle = { 'line-color': color, 'source-arrow-color': color, 'target-arrow-color': color, 'width': 3 };
			api.changeHighlightStyle(idx, nodeStyle, edgeStyle);

			const opt = document.getElementById('highlightColors').options[idx];
			opt.value = color;
			opt.text = color;
		});

		document.getElementById('addColorBtn').addEventListener('click', () => {
			let color = getRandomColor();
			let nodeStyle = { 'border-color': color, 'border-width': 3 };
			let edgeStyle = { 'line-color': color, 'source-arrow-color': color, 'target-arrow-color': color, 'width': 3 };
			api.addHighlightStyle(nodeStyle, edgeStyle);
			addHighlightColorOptions();
			document.getElementById('highlightColors').selectedIndex = api.getHighlightStyles().length - 1;
			document.getElementById('colorInp').value = color;
		});

		addHighlightColorOptions();
		const s = api.getHighlightStyles()[0];
		if (s) {
			document.getElementById('colorInp').value = s.node['border-color'];
		}
		function addHighlightColorOptions () {
			const colors = api.getHighlightStyles().map(x => x.node['border-color']);

			document.getElementById('highlightColors').innerHTML = '';

			for (let i = 0; i < colors.length; i++) {
				const o = document.createElement('option');
				o.text = colors[i];
				o.value = colors[i];
				if (i === 0) {
					o.selected = 'selected';
				}
				document.getElementById('highlightColors').appendChild(o);
			}
		}

		function getRandomColor () {
			const letters = '0123456789ABCDEF';
			let color = '#';
			for (let i = 0; i < 6; i++) {
				color += letters[Math.floor(Math.random() * 16)];
			}
			return color;
		}

		document.addEventListener('keydown', (e) => {
			if (e.ctrlKey) {
				if (e.which === 90)
					ur.undo();
				else if (e.which === 89)
					ur.redo();
			}
		});
	});

	const elements = [];
	const allItems = {};

	// Use lowercase names with spaces removed as node and edge IDs.
	const itemNameToNodeID = (name) => name.replace(/\s/g, '').replace(/\(/g, '').replace(/\)/g, '').toLowerCase();


	// Add all items to an object so we can use it as a hashmap to see if an item doesn't already exist as a node.
	items.forEach((item) => {
		allItems[itemNameToNodeID(item.name)] = true;
	});

	// Create node & edge data structures
	items.forEach((item) => {
		let nodeID;

		// Create nodes
		try {
			nodeID = itemNameToNodeID(item.name);

			log.verbose(`Adding node for ${item.name}`);
			elements.push({
				group: 'nodes',
				classes: item.recipes ? ['crafted'] : ['root'],
				data: {
					id: nodeID,
					name: item.name,
					// imageUrl: item.image,
					color: 'green',
					category: item.category
				}
			});
		} catch (e) {
			log.error(`Problem adding node for ${item.name}`);
			log.error(e);
		}

		// Create edges
		if (item.recipes && item.recipes.length) {
			item.recipes.forEach((recipe) => {

				try {
					recipe.ingredients.forEach((ingredient) => {
						const ingredientNodeID = itemNameToNodeID(ingredient.name);

						// If an ingredient doesn't exist as a node, then add it.
						if (!allItems[ingredientNodeID]) {
							log.warn(`${ingredient.name} does not exist as a node on the graph.`);

							elements.push({
								group: 'nodes',
								classes: ['root'],
								data: {
									id: ingredientNodeID,
									name: ingredient.name,
									color: 'red',
									category: 'uncategorized'
								}
							});
						}

						log.verbose(`Adding edge ${ingredient.name} -> ${item.name}`);
						elements.push({
							group: 'edges',
							data: {
								id: `${ingredientNodeID}->${nodeID}`,
								source: ingredientNodeID,
								target: nodeID
							}
						});
					});
				} catch (e) {
					log.error(`Problem adding edge for recipe: ${JSON.stringify(recipe)}`);
					log.error(e);
				}
			});
		}
	});

	return (
		<CytoscapeComponent
			global='cy'
			cy={(cy) => { cyRef = cy; }}
			elements={elements}
			stylesheet={[
				{
					selector: 'node',
					style: {
						// 'background-image': 'data(imageUrl)',
						'content': 'data(name)',
						'border-color': 'lightgrey',
						'border-width': 3,
						'background-color': 'data(color)'
					}
				},
				{
					selector: 'edge',
					style: {
						'curve-style': 'straight',
						'target-arrow-shape': 'triangle'
					}
				},
				{
					selector: 'edge:selected',
					style: {
						'line-color': 'black',
						'source-arrow-color': 'black',
						'target-arrow-color': 'black',
					}
				},
				{
					selector: 'node:selected',
					style: {
						'border-color': 'black',
						'border-width': '3px',
						'background-color': 'grey'
					}
				},
				{
					selector: 'node:parent',
					style: {
						'background-color': 'green'
					}
				}
			]}
			style={{ width: '100%', height: '100%' }}
		/>
	);
};

export default CyComponent;
