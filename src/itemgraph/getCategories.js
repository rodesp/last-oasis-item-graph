const items = require('../items.json');

const categories = items.reduce(
	(categories, item) => !categories.includes(item.category) ? categories.concat(item.category) : categories,
	[]
);

const techTreeCategories = {
	vitamins: 0,
	equipment: 0,
	crafting: 0,
	construction: 0,
	walkers: 0,
	notUnlockable: 0
};

items.forEach((item) => {
	const cat = item.category;
	try {
		switch (true) {
			case !Array.isArray(item.recipes):
				techTreeCategories.notUnlockable++;
				break;
			case /vitamins/.test(cat) || /Cauterizing/.test(item.name):
				techTreeCategories.vitamins++;
				item.recipes[0].techTreeCategory = 'Vitamins';
				item.ttc = 'Vitamins';
				break;
			case /armors/.test(cat) || /gear/.test(cat) || /items\/weapons/.test(cat) || /equipment/.test(cat) || /interactables/.test(cat):
				techTreeCategories.equipment++;
				item.recipes[0].techTreeCategory = 'Equipment';
				item.ttc = 'Equipment';
				break;
			case /station/.test(cat) || /materials/.test(cat):
				techTreeCategories.crafting++;
				item.recipes[0].techTreeCategory = 'Crafting';
				item.ttc = 'Crafting';
				break;
			case /buildings/.test(cat) || /ammo/.test(cat):
				techTreeCategories.construction++;
				item.recipes[0].techTreeCategory = 'Construction';
				item.ttc = 'Construction';
				break;
			case /walkers/.test(cat):
				techTreeCategories.walkers++;
				item.recipes[0].techTreeCategory = 'Walkers';
				item.ttc = 'Walkers';
				break;
			default:
				console.log(item);
		}
	} catch (e) {
		console.log(item);
		console.error(e);
	}
});

// console.log(categories);
console.log(JSON.stringify(items));
console.log(techTreeCategories);