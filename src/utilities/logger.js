import ulog from 'ulog';

const log = ulog('last-oasis-item-graph');
log.level = log.INFO;

export default log;
