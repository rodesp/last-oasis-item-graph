import './graphController.css';
import './Autosuggest.css';

import React, { useEffect, useState } from 'react';
import Autosuggest from 'react-autosuggest';

import log from '../utilities/logger';

const getSuggestionValue = suggestion => suggestion;

const renderSuggestion = suggestion => <span>{suggestion}</span>;

const GraphController = (props) => {
	const [suggestions, setSuggestions] = useState([]);
	const [value, setValue] = useState('');
	const items = props.items;

	const getSuggestions = ({ value }) => {
		// Split search term into separate words
		const terms = value.trim().toLowerCase().split(' ');

		// Returns every item whose name contains every word being searched for
		return items.filter(itemName => terms.every(term => itemName.toLowerCase().includes(term)));
	}

	const onSuggestionsFetchRequested = (value) => setSuggestions(getSuggestions(value));
	const onSuggestionsClearRequested = () => setSuggestions([]);
	const onChange = (event, { newValue }) => {
		setValue(newValue);
	}

	const inputProps = {
		placeholder: 'Type an item\'s name',
		value,
		onChange
	}

	return (
		<div id='sidebar'>
			<label htmlFor='searchBox'>Enter the name of an item</label><br />
			<Autosuggest
				id='searchBox'
				suggestions={suggestions}
				onSuggestionsFetchRequested={onSuggestionsFetchRequested}
				onSuggestionsClearRequested={onSuggestionsClearRequested}
				getSuggestionValue={getSuggestionValue}
				renderSuggestion={renderSuggestion}
				inputProps={inputProps}
			/>
			<input type='button' id='searchButton' value='Search'
				onClick={(e) => {
					// TODO: Consolidate this with the function on line 249 of itemGraph.jsx
					const selectedEles = window.cy.$(`#${value.replace(/\s/g, '').replace(/\(/g, '').replace(/\)/g, '').toLowerCase()}`);
					window.cy.api.zoomToSelected(selectedEles);
				}} />

			<dl>
				<dt>Click and Drag</dt>
				<dd>to move the graph around.</dd>
				<dt>Click on a node</dt>
				<dd>to select it.</dd>
				<dt>Double click on a node</dt>
				<dd>to reveal hidden parents.</dd>
				<dt>Shift + Hold Click</dt>
				<dd>to select neighbors.</dd>
				<dt>Shift + Click and Drag</dt>
				<dd>to select a collection of nodes.</dd>
				<dt>Mouse wheel</dt>
				<dd>to zoom.</dd>
				<dt>CTRL+Z / CTRL+Y</dt>
				<dd>Undo / Redo</dd>
			</dl>

			<input type='button' id='hide' value='Hide Selected' />
			<input type='button' id='hideUnselected' value='Hide Unselected' />
			<input type='button' id='showAll' value='Show All' />
			<br /><br />
			<input type='button' id='showHiddenNeighbors' value='Show Hidden Neighbors of Selected' />
			<br /><br />

			<label htmlFor='highlightColors'>Highlight Color:</label>
			<div className='m-1'>
				<select id='highlightColors'>
				</select>
				<input type='color' id='colorInp' />
				<input type='button' id='colorChangerBtn' value='Update Color' />
				<input type='button' id='addColorBtn' value='Add Color' />
			</div>
			<br />

			<input type='button' id='highlightElements' value='Highlight Selected' />
			<input type='button' id='highlightNeighbors' value='Highlight Neighbors' /><br /><br />
			<input type='button' id='removeSelectedHighlights' value='Remove Selected Highlights' />
			<input type='button' id='removeAllHighlights' value='Remove All Highlights' /> <br /><br />
			<input type='button' id='marqueeZoom' value='Marquee Zoom' />
			<br />
			<span>Shift + drag to specify region; gets
            disabled after zoom, mouse events on canvas and other api functions calls</span>
			<br /><br />
			<label htmlFor='layout'>Rearrange on Hide/Show</label>
			<input type='checkbox' id='layout' name='layout' defaultChecked={true} />
			<br /><br />
		</div>
	)
}

export default GraphController;