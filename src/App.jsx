import './App.css'

import React from 'react';

import GraphController from './graphController/graphController';
import CyComponent from './itemgraph/itemGraph';
import items from './items.json';

function App () {
  return (
    <React.Fragment>
      <h1>Last Oasis Item Graph</h1>
      <div id='graphController'>
        <GraphController items={items.map((item) => item.name)} />
      </div>
      <div id='graphContainer'>
        <CyComponent items={items} />
      </div>
    </React.Fragment >
  )
}

export default App;
